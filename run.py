import random
from flask import Flask, jsonify, request
from flask_cors import CORS
from os import listdir, path, getenv
import base64
from app import exception
from app.src.security.security import check_token
from app.src.utils.utils import build_filter_from_request, build_images_from_request
from app.src.detect.inference import filter_images_with_model
from dotenv import load_dotenv

app = Flask(__name__)
CORS(app)

IMAGES_PATH = 'app/data/dataset/images/test'
app.config['UPLOAD_FOLDER'] = IMAGES_PATH

load_dotenv(override=True)

METHOD_OPTIONS = 'OPTIONS'

@app.errorhandler(exception.Unauthorized)
def unauthorized(e):
    return jsonify({"message": "ERROR: Unauthorized"}), 401


@app.before_request
def before_request():
    if request.url_rule.rule != '/' and METHOD_OPTIONS != request.method:  # Skip root url
        check_token(request)


@app.after_request
def after_request(response):
    headers = [
        "Access-Control-Allow-Headers",
        "Access-Control-Allow-Methods",
        "Access-Control-Allow-Origin"
    ]

    for header in headers:
        if header in response.headers:
            continue
        response.headers.add(header, "*")

    return response


@app.route('/')
def root():
    return {'TFM': 'Who is who'}


@app.route('/health')
def health():
    return 'OK'


@app.route('/api/get_images')
def get_images():
    images_list = listdir(IMAGES_PATH)

    images = []
    for image in random.sample(images_list, k=6):
        with open(IMAGES_PATH + '/' + image, "rb") as img_file:
            b64_string = base64.b64encode(img_file.read())

        images.append(
            {
                'name': image,
                'data': 'data:image/jpeg;base64,' + str(b64_string).split('\'')[1]
            }
        )

    return jsonify(images)


@app.route('/api/filter_images', methods=['POST'])
def filter_images():
    filter = build_filter_from_request(request)
    images = build_images_from_request(request)

    images_filter = filter_images_with_model(images, filter)

    new_images = [i.to_dict() for i in images_filter['images']]
    images_deleted = [i.to_dict() for i in images_filter['images_deleted']]

    return jsonify(
        {
            'images': new_images,
            'images_deleted': images_deleted,
            'total_images': len(new_images),
            'total_images_deleted': len(images_deleted)
        }
    )


@app.route('/api/upload_image', methods=['POST'])
def upload_image():
    file = request.files.get('image')
    file.save(path.join(app.config['UPLOAD_FOLDER'], file.filename))
    return ''


if __name__ == '__main__':
    from waitress import serve

    DEBUG = (getenv('debug', 'False') == 'True')
    HOST = getenv('host', '0.0.0.0')
    PORT = getenv('port', 8080)

    if DEBUG:
        app.run(debug=DEBUG, host=HOST, port=PORT)
    else:
        serve(app, host=HOST, port=PORT)
