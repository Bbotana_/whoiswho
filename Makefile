VENV = whoIsWhoEnv
PYTHON = $(VENV)/bin/python3
PIP = $(VENV)/bin/pip

.PHONY: run clean

run: $(VENV)/bin/activate
	$(PYTHON) run.py

$(VENV)/bin/activate: requirements.txt
	python3 -m venv $(VENV)
	$(PIP) install -r requirements.txt

setup:
	python3 -m venv $(VENV)
	$(PIP) install -r requirements.txt

clean:
	rm -rf __pycache__
	rm -rf $(VENV)

train: $(VENV)/bin/activate
	$(PYTHON) app/src/models/train_model.py

detect: $(VENV)/bin/activate
	$(PYTHON) app/src/detect/detect.py

val: $(VENV)/bin/activate
	$(PYTHON) app/src/evaluation/evaluate_model.py