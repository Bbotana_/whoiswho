# Who is Who (UEM - TFM 2021-2022)

## Install
```
make setup
```

## Run web server (and install)
```
make run
```

## Execute detect example
```
make detect-example
```


