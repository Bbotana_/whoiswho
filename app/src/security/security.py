import os

from dotenv import load_dotenv
from flask import request

from app import exception

load_dotenv()

token = os.environ.get('api-token')


def check_token(data: request) -> None:
    auth = data.headers.get("X-Api-Key")
    if auth != token:
        raise exception.Unauthorized()
