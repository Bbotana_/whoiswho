from app.src.enums.ObjectType import ObjectType


class ObjectFilter:
    def __init__(self, object_type: ObjectType, value: bool):
        self.__object_type = object_type
        self.__value = value

    def object_type(self) -> ObjectType:
        return self.__object_type

    def value(self) -> bool:
        return self.__value
