from app.src.entities.Object import Object


class Image:
    def __init__(self, path: str):
        self.__path = path
        self.__name = path.split('/')[-1]
        self.__objects = []
        self.__data: str = ''

    def path(self) -> str:
        return self.__path

    def objects(self):
        return self.__objects

    def name(self) -> str:
        return self.__name

    def data(self) -> str:
        return self.__data

    def set_data(self, data: str):
        self.__data = data

    def set_objects(self, objects) -> None:
        self.__objects = objects

    def to_dict(self) -> dict:
        return {
            'path': self.__path,
            'name': self.__name,
            'data': self.__data,
            'objects': [o.to_dict() for o in self.__objects]
        }
