from app.src.entities.ObjectFilter import ObjectFilter


class Filter:
    def __init__(self, object_types = []):
        self.__object_types = object_types

    def object_types(self):
        return self.__object_types
