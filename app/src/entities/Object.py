from app.src.enums.ObjectType import ObjectType


class Object:
    def __init__(self, name: ObjectType, conf: float):
        self.__name = name
        self.__conf = conf

    def name(self):
        return self.__name

    def conf(self):
        return self.__conf

    def to_dict(self):
        return {
            'name': self.__name,
            'conf': self.__conf,
        }
