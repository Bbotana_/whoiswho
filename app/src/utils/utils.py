import base64
from pathlib import Path

from flask import Request

from app.src.entities.Filter import Filter
from app.src.entities.ObjectFilter import ObjectFilter
from app.src.entities.Image import Image


def get_file_name_from_path(path: str):
    return Path(path).name


def read_picture_from_path_and_parse_base64(path: str) -> str:
    with open(path, "rb") as img_file:
        b64_string = base64.b64encode(img_file.read())

    return 'data:image/jpeg;base64,' + str(b64_string).split('\'')[1]


def build_filter_from_request(request: Request) -> Filter:
    data = request.get_json()

    if 'filter' in data and 'object_types' in data['filter']:
        object_types = []
        for object_type_data in data['filter']['object_types']:
            object_types.append(
                ObjectFilter(
                    object_type_data['object_type'],
                    object_type_data['value'],
                )
            )

        return Filter(object_types)

    return Filter()


def build_images_from_request(request: Request):
    data = request.get_json()
    path = 'app/data/dataset/images/test/'

    if 'images' not in data:
        return None

    images = []
    for image_data in data['images']:
        images.append(
            Image(
                path + image_data['name']
            )
        )

    return images
