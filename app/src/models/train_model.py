from yolov5 import train
import os
import yaml


def update_dataset_path():
    with open('app/data/data.yaml') as f:
        doc = yaml.load(f, Loader=yaml.SafeLoader)

    doc['path'] = os.path.abspath('app/data/dataset')

    with open('app/data/data.yaml', 'w') as f:
        yaml.dump(doc, f, sort_keys=False)


update_dataset_path()

train.run(
    hyp='app/src/models/hyp/hyp.yaml',
    data='app/data/data.yaml',
    project='app/src/models/runs',
    imgsz=640,
    batch=4,
    epochs=100,
    weights='yolov5s.pt'
)
