from yolov5 import val
import os
import yaml


def update_dataset_path():
    with open('app/data/data.yaml') as f:
        doc = yaml.load(f, Loader=yaml.SafeLoader)

    doc['path'] = os.path.abspath('app/data/dataset')

    with open('app/data/data.yaml', 'w') as f:
        yaml.dump(doc, f, sort_keys=False)


update_dataset_path()

val.run(
    data='app/data/data.yaml',
    weights='app/models/runs/exp2/weights/best.pt',
    project='app/src/evaluation/runs',
)