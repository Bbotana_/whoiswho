from enum import Enum


class ObjectType(Enum):
    BEARD = 'beard'
    GLASSES = 'glasses'
    MASK = 'mask'
    HAT = 'hat'
    CIGARETTE = 'cigarette'
