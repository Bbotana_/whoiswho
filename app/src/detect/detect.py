from yolov5 import detect


detect.run(
    weights='app/models/runs/exp2/weights/best.pt',
    source='app/data/dataset/images/test',
    project='app/src/detect/runs',
)