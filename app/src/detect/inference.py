import tempfile
from os import listdir

from yolov5 import load
from yolov5.models.common import AutoShape
from app.src.entities.Filter import Filter
from app.src.entities.Image import Image
from app.src.entities.Object import Object
from app.src.utils.utils import read_picture_from_path_and_parse_base64

MODEL_BASE_PATH = 'app/src/models/runs'


def filter_images_with_model(images: list, filter_data: Filter) -> dict:
    images_filter = []
    images_deleted = []

    objects_true = [o.object_type() for o in filter_data.object_types() if True == o.value()]
    objects_false = [o.object_type() for o in filter_data.object_types() if False == o.value()]

    for image in images:
        image = __fill_objects_and_data_image(image)
        labels = [o.name() for o in image.objects()]

        if __check_include_all_objects(objects_true, labels) and __check_include_all_objects(objects_false, labels, True):
            images_filter.append(image)
        else:
            images_deleted.append(image)

    return {
        'images': images_filter,
        'images_deleted': images_deleted
    }


def __check_include_all_objects(objects: list, labels: list, inverse=False) -> bool:
    for o in objects:
        if (not(o in labels) and not inverse) or (o in labels and inverse):
            return False

    return True


def __get_model() -> AutoShape:
    last_model_folder = listdir(MODEL_BASE_PATH)[-1]
    return load(MODEL_BASE_PATH + '/' + last_model_folder + '/' + '/weights/best.pt')


def __fill_objects_and_data_image(image: Image, conf=0.1) -> Image:
    results = __get_model()(image.path())

    tmp_dir = tempfile.gettempdir()
    results.save(save_dir=tmp_dir, labels=False)
    image.set_data(read_picture_from_path_and_parse_base64(tmp_dir + '/' + image.name()))

    objects = []
    for f in list(filter(lambda d: d['conf'] > conf, results.crop())):
        objects.append(Object(
            f['label'].split(' ')[0],
            float(f['conf'])
        ))

    image.set_objects(objects)

    return image
